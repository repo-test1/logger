import * as winston from 'winston';
import * as Transport from 'winston-transport';

import {config} from '../config';

// TODO log to file
class Logger {
    private static readonly dateTimeFormat = 'YYYY-MM-DD HH:mm:ss.SSS';

    private readonly tag: string;
    private readonly level: string;
    private readonly logger: winston.Logger;

    constructor(tag: string, level?: string) {

        this.tag = tag;
        this.level = level ?? config.log.defaultLevel;

        const transports: Transport[] = [new winston.transports.Console()];

        const format = winston.format;
        this.logger = winston.createLogger({
            level: this.level,
            format: format.combine(
                format.colorize({all: true}),
                format.timestamp({format: Logger.dateTimeFormat}),
                format.label({label: this.tag}),
                format.errors({stack: false}),
                format.splat(),
                format.printf((info) => {
                    // eg.: 2099-02-23 23:59:59.999 - info: app - App launched. PID: 99999
                    return `${info.timestamp} - ${process.pid} - ${info.level} - ${info.label} - ${info.message}`;
                }),
            ),
            defaultMeta: {
                service: this.tag,
            },
            transports
        });
    }

    joinMsg(messages: any[]) {
        let result = '';
        for (let msg of messages) {
            // str num ...
            if (typeof msg !== 'object') {
                result += msg.toString();
            } else {
                result += JSON.stringify(msg);
            }
            result += ' ';
        }
        return result;
    }

    error(...messages: any[]) {
        const msg = this.joinMsg(messages);
        this.logger.error(msg);
    }

    warn(...messages: any[]) {
        const msg = this.joinMsg(messages);
        this.logger.warn(msg);
    }

    info(...messages: any[]) {
        const msg = this.joinMsg(messages);
        this.logger.info(msg);
    }

    debug(...messages: any[]) {
        const msg = this.joinMsg(messages);
        this.logger.debug(msg);
    }
}

function getLogger(tag: string, level?: string) {
    return new Logger(tag, level);
}

export {
    getLogger,
};
